package Gioco;

import javax.swing.*;

public class Main {

	private static final String WINDOWS_NAME = "super_pilar";
	private static final int DEFAULT_WIDTH = 700;
	private static final int DEFAULT_HEIGHT = 360;

	public  static Piattaforma scene;

	public static void main(String[] args) {

		//finestra della mia applicazione
		JFrame finestra = new JFrame(WINDOWS_NAME);
		finestra.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		finestra.setSize(DEFAULT_WIDTH ,DEFAULT_HEIGHT);
		finestra.setLocationRelativeTo(null);
		finestra.setResizable(true);
		finestra.setAlwaysOnTop(true);


		//aggiungiamo la piattaforma
		scene = new Piattaforma();
		finestra.setContentPane(scene);
		finestra.setVisible(true);

		// collegamento con la classe movimenti
		Thread cronometro = new Thread(new Refresh());
		cronometro.start();
	}

}
