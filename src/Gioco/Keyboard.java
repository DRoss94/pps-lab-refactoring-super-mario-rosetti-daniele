package Gioco;

import com.sun.org.apache.bcel.internal.generic.MONITORENTER;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {


	private static final int DEFAULT_X1 = -50;
	private static final int DEFAULT_X2 = 750;

	@Override
	public void keyPressed(KeyEvent e) {
		
		if(Main.scene.mario.isVivo()){
			if(e.getKeyCode() == KeyEvent.VK_RIGHT){
				this.movementRight();
			}else if(e.getKeyCode() == KeyEvent.VK_LEFT){
				this.movementLeft();
			}
			if(e.getKeyCode() == KeyEvent.VK_UP){
				this.jump();
			}
		}
	}

	private void movementRight() {
		this.movement(-1,0,true, 1);
	}

	private void movementLeft() {
		this.movement(4601, 4600, false, -1);
	}

	private void movement(int currentX, int destinationX, boolean isRight, int movement) {
		if(Main.scene.getxPos() == currentX){
			Main.scene.setxPos(destinationX);
			Main.scene.setX1(DEFAULT_X1);
			Main.scene.setX2(DEFAULT_X2);
		}
		Main.scene.mario.setIn_movimento(true);
		Main.scene.mario.setVerso_destra(isRight);
		Main.scene.setMov(movement); // si muove verso sinistra
	}

	private void jump() {
		Main.scene.mario.setJump(true);
		Audio.playSound("/audio/jump.wav");
	}

	@Override
	public void keyReleased(KeyEvent e) {
		Main.scene.mario.setIn_movimento(false);
		Main.scene.setMov(0);
	}

	@Override
	public void keyTyped(KeyEvent e) {}

}
